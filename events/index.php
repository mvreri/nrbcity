<?php
//echo $root_url;
if (isset($_GET['event'])) {
	$event = $_GET['event'];
	$queryString = "&event=$event";
	//echo $event;
} else {
	$event = '';
	$queryString = '';
	header('Location: index.php?error=' . urlencode('No Ref No. for the event')); 
}
//creating artificial paragraphs
function TrimString($String, $Length){
	if(strlen($String)<=$Length){
		$stringValue=$String;
		} else {
			$new_l = strpos ($String , ".", $Length);
			$Length = $new_l+1;
			$stringValue=substr($String,0,$Length);
			}
			return $stringValue;
			}
	$m=50;
	$conn = pg_connect("host=46.101.3.7 port= 5432 dbname=events user=zerochills password=zer0chills");
	$r = pg_query("SELECT eid, title,description, start_time, stop_time, olson_path, venue_id, venue_url, venue_name, venue_address, city_name, region_name, region_abbr, postal_code, country_name, country_abbr2, country_abbr, latitude, longitude, geocode_type, all_day, recur_string, calendar_count, comment_count, link_count, going_count, watching_count, created, owner, modified, performers, imagesmall, imagemedium, privacy, calendars, groups, going
					FROM events WHERE  start_time::date >= current_date
					OR (stop_time <> '' AND stop_time::date>=current_date )
					AND eid='".$event."'
					ORDER BY start_time::date asc;");
	$row=pg_fetch_assoc($r);
	extract($row);
?>
<!DOCTYPE HTML>

<html>
	<head>
	<head>
		<title>nrb.city | events in Nairobi</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="nrb.city is an online platform that leverages on the power of crowdsourcing traffic, weather, news and event information from social media sites and third party applications and puts them in one place. Easily find something of interest in Nairobi, ranging from the stock exchange values, to the current news, to the events lined up. Nairobi just got simpler." />
		<meta name="keywords" content="Nairobi, Nairobi News, Events in Nairobi, Weather in Nairobi, Nairobi Weather, Kenya, Finance In Kenya, Traffic in Nairobi, Traffic Jam In Nairobi, Matatu Culture, Nairobi Culture" />
		<meta name="author" content="Black | Dev & Design - http://black.co.ke" />
		<meta name="ROBOTS" content="INDEX, FOLLOW" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<link rel="shortcut icon" href="../images/logo_smallest.png"/>
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<span class="logo"><img src="../images/logo_smallest.png" alt="nrb.city | Nairobi City" /></span>
						<h1>nrb.city </h1>
						<p>nrb.city.events</p>
					</header>
					<!-- Nav -->
					<nav id="nav">
						<ul>
							<li><a href="../">Home</a></li>
							<li><a href="#content" class="active">The Event</a></li>							
							<li><a href="../maps">Find Places</a></li>				
						</ul>
					</nav>

				<!-- Main -->
					<div id="main">

						<!-- Content -->
							<section id="content" class="main">

								<!-- Table -->
									<section>
										<h2><?php echo $title; ?></h2>
															
										<div class="table-wrapper">
											<table>
												<thead>
													<tr>
														<th></th>														
														<th>Event Details</th>														
														
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Name</td>
														<td><?php echo $title; ?></td>
													</tr>
													<tr>
														<td>About</td>
														<td><?php echo $description; ?></td>
													</tr>
													<tr>
													<td>Start Date</td>
														<td><?php echo date("d F, Y",  strtotime($start_time)); ?> at <?php echo date("H:i ",  strtotime($start_time)); ?>H</td>
													</tr>
													<tr>
													<td>Address</td>
														<td><?php echo $venue_address; ?></td>
													</tr>
													<tr>
													<td>Venue</td>
														<td><?php echo $venue_name; ?></td>
													</tr>
													
													<tr>
													<td>Region</td>
														<td><?php echo $region_name.' '.$city_name.' '; ?></td>
													</tr>
													<tr>
													<td></td>
														<td><span class="image" align="center"><?php echo '<img src="'.$imagemedium.'" alt="'.str_replace("-|-","'",$title).'">';?></span></td>
													</tr>
													<tr>
													<td></td>
														<td><a href="#">Map It</a></td>
													</tr>
													
											</table>
										</div>
										<iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $latitude; ?>%2C%20<?php echo $longitude; ?>&key=AIzaSyDtUu3Mg6Vy7b2lhsub3iptL4Cj9WiiM8g" allowfullscreen></iframe>
									
									</section>
							</section>

					</div>

				<!-- Footer -->
					<footer id="footer">
						<section>
							<h2>Feedback</h2>
							<p>Have any suggestions, contributions, feedback or recommendations? We'd love to hear from you. </p>
							<ul class="actions">
								<li><a href="#" class="button">Contact Us</a></li>
							</ul>
						</section>
						<section>
							<h2>Contacts</h2>
							<dl class="alt">
								<dt>Address</dt>
								<dd>Westlands Business Centre, Nairobi</dd>
								<dt>Phone</dt>
								<dd>+254 795 11 40 43</dd>
								<dt>Email</dt>
								<dd><a href="#">info@nrb.city</a></dd>
							</dl>
							<ul class="icons">
								<li><a href="#" class="icon fa-twitter alt"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook alt"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram alt"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon fa-github alt"><span class="label">GitHub</span></a></li>
								<li><a href="#" class="icon fa-dribbble alt"><span class="label">Dribbble</span></a></li>
							</ul>
						</section>
						<p class="copyright">&copy; nrb.city by <a href="https://black.co.ke">Black | Dev & Design</a>.</p>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.scrollex.min.js"></script>
			<script src="../assets/js/jquery.scrolly.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="../assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>

	</body>
</html>