<!DOCTYPE HTML>

<html>
	<head>
		<title><?php echo $pageTitle; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="nrb.city is an online platform that leverages on the power of crowdsourcing traffic, weather, news and event information from social media sites and third party applications and puts them in one place. Easily find something of interest in Nairobi, ranging from the stock exchange values, to the current news, to the events lined up. Nairobi just got simpler." />
		<meta name="keywords" content="Nairobi, Nairobi News, Events in Nairobi, Weather in Nairobi, Nairobi Weather, Kenya, Finance In Kenya, Traffic in Nairobi, Traffic Jam In Nairobi, Matatu Culture, Nairobi Culture" />
		<meta name="author" content="Black | Dev & Design - http://black.co.ke" />
		<meta name="ROBOTS" content="INDEX, FOLLOW" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" type="text/css" href="maps/styles.css" />
		<link rel="stylesheet" href="assets/css/main.css" />

		<link href="starter-template.css" rel="stylesheet">
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<link rel="shortcut icon" href="images/logo_smallest.png"/>
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<span class="logo"><img src="images/logo_smallest.png" alt="nrb.city | Nairobi City" /></span>
						<h1>nrb.city </h1>
						<p>A 360&deg; view of the city in the sun, the hub of East Africa.<br />
						Built and tweaked by <a href="http://black.co.ke" target="_blank">Black</a>.</p>
					</header>

				<!-- Nav -->
					<nav id="nav">
						<ul>
							<li><a href="#intro" class="active">Intro</a></li>
							<li><a href="maps">Find Places</a></li>
							<li><a href="#find">Search Tweets</a></li>
							<li><a href="#news" >News</a></li>
							<li><a href="#events">Events</a></li>
							<li><a href="#weather">Weather</a></li>
							<li><a href="#finance">Finance</a></li>
							<li><a href="#traffic">Traffic</a></li>
						</ul>
					</nav>

				<!-- Main -->
					<div id="main">

						<!-- Introduction -->
							<?php require_once $intro; ?>

						<!-- Find In -->
							<section id="find" class="main special">
								<header class="major">
									<h2>Search | find.in.nairobi</h2>
								</header>
								<div class="table-wrapper">
									<?php require_once $find; ?>
								</div>	
								<footer class="major">
									<ul class="actions">
										<li><a href="maps/#content" class="button">SEARCH MAPS</a></li>
									</ul>
								</footer>
							</section>
							
							
						<!-- News -->
							<section id="news" class="main special">
								<header class="major">
									<h2>News</h2>
								</header>
								
								<ul class="alt">
													
													
												
									<?php 
										
										$m=50;
										$conn = pg_connect("host=46.101.3.7 port= 5432 dbname=news user=zerochills password=zer0chills");
										$r = pg_query("SELECT nfid,sourceid,title,description, link, date 
														FROM newsfeed 
														where date::date >= current_date-interval '3' day
														and description <> ''
														ORDER BY date::date desc;");
										while ($row=pg_fetch_assoc($r)){
											extract($row);
											?>
											<li><?php echo '<strong>'.str_replace("-|-","'",$title).'</strong><br>( Reported on: '. date("d F, Y",  strtotime($date)).')<br><p>'.str_replace("-|-","'",$description).'<br><a href="news?article='.$nfid.'" target="_blank">Read More</a></p>'; ?></li>
									<?php
										}
									?>
									</ul>
																		
							</section>
							
						<!-- Events -->
							<section id="events" class="main special">
								<header class="major">
									<h2>Events</h2>
								</header>
								<div class="content">
								<ul class="alt">
													
													
												
									<?php 
										//creating artificial paragraphs
										function TrimString($String, $Length){
											if(strlen($String)<=$Length){
												$stringValue=$String;
											} else {
												$new_l = strpos ($String , ".", $Length);
												$Length = $new_l+1;
												$stringValue=substr($String,0,$Length);
											}
											return $stringValue;
										}
										$m=50;
										$conn = pg_connect("host=46.101.3.7 port= 5432 dbname=events user=zerochills password=zer0chills");
										$r = pg_query("SELECT eid, title,description, start_time, stop_time, olson_path, venue_id, venue_url, venue_name, venue_address, city_name, region_name, region_abbr, postal_code, country_name, country_abbr2, country_abbr, latitude, longitude, geocode_type, all_day, recur_string, calendar_count, comment_count, link_count, going_count, watching_count, created, owner, modified, performers, imagesmall, imagemedium, privacy, calendars, groups, going
														FROM events WHERE  start_time::date >= current_date
														OR (stop_time <> '' AND stop_time::date>=current_date )
														ORDER BY start_time::date asc;");
										while ($row=pg_fetch_assoc($r)){
											extract($row);
											?>
											<li><?php echo '<strong>'.str_replace("-|-","'",$title).'</strong><br>( Starts: '. date("d F, Y",  strtotime($start_time)).')<br><p>'.TrimString(str_replace("-|-","'",$description), $m).'<br><a href="events?event='.$eid.'" target="_blank">Read More</a></p>'; ?><span class="image"><?php echo '<img src="'.$imagemedium.'" alt="'.str_replace("-|-","'",$title).'">';?></span></li>
									<?php
										}
									?>
									</ul>
								</div>
									<div class="eventful-badge eventful-small">
									  <img src="http://api.eventful.com/images/powered/eventful_58x20.gif"
										alt="Local Events, Concerts, Tickets">
									  <p><a href="http://eventful.com/">Events</a> by Eventful</p>
									</div>
							</section>

						<!-- Second Section -->
							<section id="weather" class="main special">
							<div id="weather" class="table-wrapper"></div>
								<?php require_once $weather; ?>
								
							</section>

						<!-- Finance -->
							<section id="finance" class="main special">
							<div id="loading">
							  <p align="center"><img src="images/finance.gif" /> <br>Buffering Finance...<br> <?php //while (1==1) {    echo random();    sleep(5); } ?></p>
							</div>
								<div id="finance" class="table-wrapper"></div>										
							</section>
							
							

						<!-- Traffic -->
							<section id="traffic" class="main special">
							
										
							<div id="loading">
							  <p align="center"><img src="images/traffic.gif" /> <br>Please Wait. There seems to be a bit of traffic...<br> <?php //while (1==1) {    echo random();    sleep(5); } ?></p>
							</div>							
								<div id="traffic" class="table-wrapper"></div>										
							</section>

					</div>

				<!-- Footer -->
					<footer id="footer">
						<section>
							<h2>Feedback</h2>
							<p>Have any suggestions, contributions, feedback or recommendations? We'd love to hear from you. </p>
							<ul class="actions">
								<li><a href="#" class="button">Contact Us</a></li>
							</ul>
						</section>
						<section>
							<h2>Contacts</h2>
							<dl class="alt">
								<dt>Address</dt>
								<dd>Westlands Business Centre, Nairobi</dd>
								<dt>Phone</dt>
								<dd>+254 795 11 40 43</dd>
								<dt>Email</dt>
								<dd><a href="#">info@nrb.city</a></dd>
							</dl>
							<ul class="icons">
								<li><a href="#" class="icon fa-twitter alt"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook alt"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram alt"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon fa-github alt"><span class="label">GitHub</span></a></li>
								<li><a href="#" class="icon fa-dribbble alt"><span class="label">Dribbble</span></a></li>
							</ul>
						</section>
						<p class="copyright">&copy; nrb.city by <a href="https://black.co.ke">Black | Dev & Design</a>.</p>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			<script src="assets/js/custom_refresh.js"></script>
			


	</body>
</html>