<!DOCTYPE html>
    <html> 
    <head> 
       <meta http-equiv="content-type" content="text/html; charset=UTF-8"/> 
       <title>Google Maps API v3 Directions Example</title> 
       <script type="text/javascript" 
               src="http://maps.google.com/maps/api/js?key=AIzaSyDtUu3Mg6Vy7b2lhsub3iptL4Cj9WiiM8g&amp;libraries=directions"></script>
    </head> 
    <body style="font-family: Arial; font-size: 12px;"> 
       <div style="width: 600px;">
         <div id="map" style="width: 280px; height: 900px; float: left;"></div> 
         <div id="panel" style="width: 300px; float: right;"></div> 
       </div>
       
       <script type="text/javascript"> 
    
         var directionsService = new google.maps.DirectionsService();
         var directionsDisplay = new google.maps.DirectionsRenderer();
    
         var map = new google.maps.Map(document.getElementById('map'), {
           zoom:7,
           mapTypeId: google.maps.MapTypeId.ROADMAP
         });
        
         directionsDisplay.setMap(map);
         directionsDisplay.setPanel(document.getElementById('panel'));
    
         var request = {
           origin: 'Nairobi', 
           destination: 'Kileleshwa',
           travelMode: google.maps.DirectionsTravelMode.DRIVING
         };
    
         directionsService.route(request, function(response, status) {
           if (status == google.maps.DirectionsStatus.OK) {
             directionsDisplay.setDirections(response);
           }
         });
       </script> 
    </body> 
    </html>
 Run code snippet