<!DOCTYPE HTML>

<html>
	<head>
		<title>nrb.city | Search for places in Nairobi</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="nrb.city is an online platform that leverages on the power of crowdsourcing traffic, weather, news and event information from social media sites and third party applications and puts them in one place. Easily find something of interest in Nairobi, ranging from the stock exchange values, to the current news, to the events lined up. Nairobi just got simpler." />
		<meta name="keywords" content="Nairobi, Nairobi News, Events in Nairobi, Weather in Nairobi, Nairobi Weather, Kenya, Finance In Kenya, Traffic in Nairobi, Traffic Jam In Nairobi, Matatu Culture, Nairobi Culture" />
		<meta name="author" content="Black | Dev & Design - http://black.co.ke" />
		<meta name="ROBOTS" content="INDEX, FOLLOW" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" type="text/css" href="styles.css" />
		<link rel="stylesheet" href="../assets/css/main.css" />

		<link href="starter-template.css" rel="stylesheet">
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<link rel="shortcut icon" href="../images/logo_smallest.png"/>
	</head>
	
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
					<span class="logo"><img src="../images/logo_smallest.png" alt="nrb.city | Nairobi City" /></span>
						<h1>nrb.city</h1>
						<p>Search For Places</p>
					</header>
					<nav id="nav">
						<ul>
							<li><a href="../" >Home</a></li>
							<li><a href="directions_all.php" >Directions</a></li>
							<li><a href="#content" >Search Map</a></li>
							<li><a href="#find" >Find In Nairobi</a></li>
							<li><a href="#amenities" >Search Results</a></li>
							
						</ul>
					</nav>

				<!-- Main -->
					<div id="main">

						<!-- Content -->
							<section id="content" class="main">

								<!-- Text -->
									
										<h2>Find Places in Nairobi</h2>
										<h3>Place information provided by Foursquare</h3>
										<p>To Search, drag the marker on the map or search in the search box provided below the map. A dynamic list of places is provided once you do any of these.</p>
										<div class="map_canvas"></div>
										<br>
											<form>
											  <input id="geocomplete" type="text" placeholder="Type in an address to find" value="Nairobi" />
											  <br>
											  <div style="text-align: center;">
														<input id="find" type="button" value="FIND" align="center" />
												</div>
											  
											  <fieldset>
												<input name="lat" id="lat" type="hidden" value="">									  
												<input name="lng" id="lng" type="hidden" value="">
											
												<!--<label>Formatted Address</label>
												<input name="formatted_address" type="text" value="">
											  </fieldset>-->
											  
											  <a id="reset" href="#" style="display:none;" >Reset Marker</a>
											  <br>
											</form>
											<br/>
											<div id="amenities"></div>
											<div id="data2"></div>

											<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDtUu3Mg6Vy7b2lhsub3iptL4Cj9WiiM8g&amp;libraries=places"></script>
											<script src="../assets/js/jquery.min.js"></script>											
											<script src="../assets/js/jquery.geocomplete.js"></script>
											
											<script>
											  $(function(){
											  
												var mapStyles = [
														{
															"featureType": "administrative",
															"elementType": "labels.text.fill",
															"stylers": [
																{
																	"color": "#444444"
																}
															]
														},
														{
															"featureType": "landscape",
															"elementType": "all",
															"stylers": [
																{
																	"color": "#f2f2f2"
																}
															]
														},
														{
															"featureType": "poi",
															"elementType": "all",
															"stylers": [
																{
																	"visibility": "off"
																}
															]
														},
														{
															"featureType": "road",
															"elementType": "all",
															"stylers": [
																{
																	"saturation": -100
																},
																{
																	"lightness": 45
																}
															]
														},
														{
															"featureType": "road.highway",
															"elementType": "all",
															"stylers": [
																{
																	"visibility": "simplified"
																}
															]
														},
														{
															"featureType": "road.arterial",
															"elementType": "labels.icon",
															"stylers": [
																{
																	"visibility": "off"
																}
															]
														},
														{
															"featureType": "transit",
															"elementType": "all",
															"stylers": [
																{
																	"visibility": "off"
																}
															]
														},
														{
															"featureType": "water",
															"elementType": "all",
															"stylers": [
																{
																	"color": "#4f595d"
																},
																{
																	"visibility": "on"
																}
															]
														}
													];
												

												$("#geocomplete").geocomplete({
												  map: ".map_canvas",
												  details: "form ",
												  markerOptions: {
													draggable: true
												  },
												  mapOptions: {
													styles: mapStyles
												  },
												  country: 'ke'
												});
												
												$("#geocomplete").bind("geocode:dragged", function(event, latLng){
												  $("input[name=lat]").val(latLng.lat());
												  $("input[name=lng]").val(latLng.lng());
												  
												  //alert($("#lat").val());
												   //$("#amenities").load('https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v=20160801&radius=1000&query=school');
												  
														
															var today = new Date();
															var dd = today.getDate();
															var mm = today.getMonth()+1; //January is 0!
															var yyyy = today.getFullYear();

															if(dd<10) {
																dd='0'+dd
															} 

															if(mm<10) {
																mm='0'+mm
															} 

															today = yyyy+''+mm+''+dd;
															$('#amenities').html("");
															$('#amenities').append('<br>The following are within 500 meters of here:<br>');
													  
													  
													  //restaurants
														   $.ajax({
															url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=restaurant',
															dataType: "json"
														}).success(function(data){
															//$('#amenities').append(JSON.stringify(data));
															//alert(JSON.stringify(data));	
															//alert(JSON.stringify(data));
																												
															$('#amenities').append('<br><strong>Restaurants</strong><br>');
															var links = JSON.parse(JSON.stringify(data));
															 for (var i = 0; i < links.response.venues.length; i++) {
																
																$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
																
															}  
															$('#amenities').append('</table></div>');
															
															
														});
														
														//hotels
														 $.ajax({
															url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=hotel',
															dataType: "json"
														}).success(function(data){
															//$('#amenities').append(JSON.stringify(data));
															//alert(JSON.stringify(data));	
															//alert(JSON.stringify(data));
															$('#amenities').append('<br><strong>Hotels</strong><br>');
															var links = JSON.parse(JSON.stringify(data));
															 for (var i = 0; i < links.response.venues.length; i++) {
																
																$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
																
															}  
															$('#amenities').append('</table></div>');
															
															
														});
														//banks
														$.ajax({
															url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=bank',
															dataType: "json"
														}).success(function(data){
															//$('#amenities').append(JSON.stringify(data));
															//alert(JSON.stringify(data));	
															//alert(JSON.stringify(data));
															$('#amenities').append('<br><strong>Banks</strong><br>');
															var links = JSON.parse(JSON.stringify(data));
															 for (var i = 0; i < links.response.venues.length; i++) {
																
																$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
																
															}  
															$('#amenities').append('</table></div>');
															
															
														});
														
														//mpesa shops
														$.ajax({
															url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=m-pesa',
															dataType: "json"
														}).success(function(data){
															//$('#amenities').append(JSON.stringify(data));
															//alert(JSON.stringify(data));	
															//alert(JSON.stringify(data));
															$('#amenities').append('<br><strong>M-Pesa Shops</strong><br>');
															var links = JSON.parse(JSON.stringify(data));
															 for (var i = 0; i < links.response.venues.length; i++) {
																
																$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
																
															}  
															$('#amenities').append('</table></div>');
															
															
														});
														//police stations
														$.ajax({
															url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=police',
															dataType: "json"
														}).success(function(data){
															//$('#amenities').append(JSON.stringify(data));
															//alert(JSON.stringify(data));	
															//alert(JSON.stringify(data));
															$('#amenities').append('<br><strong>Police Station</strong><br>');
															var links = JSON.parse(JSON.stringify(data));
															 for (var i = 0; i < links.response.venues.length; i++) {
																
																$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
																
															}  
															$('#amenities').append('</table></div>');  
															
															
														});
														
														//nakumatt 
														$.ajax({
															url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=nakumatt',
															dataType: "json"
														}).success(function(data){
															//$('#amenities').append(JSON.stringify(data));
															//alert(JSON.stringify(data));	
															//alert(JSON.stringify(data));
															$('#amenities').append('<br><strong>Supermarket</strong><br>');
															var links = JSON.parse(JSON.stringify(data));
															 for (var i = 0; i < links.response.venues.length; i++) {
																
																$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
																
															}  
															$('#amenities').append('</table></div>');
															
															
														});

													
												  $("#reset").show();
												});
												
												
												
												
												$("#reset").click(function(){
												  $("#geocomplete").geocomplete("resetMarker");
												  $('#amenities').html("");
												  $("#reset").hide();
												  return false;
												});
												
												$("#find").click(function(){
												  $("#geocomplete").trigger("geocode");
												  
												}).click();
												
												
												$('#find').on('click',function (e){   

													var today = new Date();
													var dd = today.getDate();
													var mm = today.getMonth()+1; //January is 0!
													var yyyy = today.getFullYear();

													if(dd<10) {
														dd='0'+dd
													} 

													if(mm<10) {
														mm='0'+mm
													} 

													today = yyyy+''+mm+''+dd;
													$('#amenities').html("");
													$('#amenities').append('<br>The following are within 500 meters of here:<br>');
											  
												   $.ajax({
													url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=restaurant',
													dataType: "json"
												}).success(function(data){
													//$('#amenities').append(JSON.stringify(data));
													//alert(JSON.stringify(data));	
													//alert(JSON.stringify(data));
																										
													$('#amenities').append('<div class="table-wrapper"><table><th><strong>Restaurants</strong></th><th></th></tr>');
													var links = JSON.parse(JSON.stringify(data));
													 for (var i = 0; i < links.response.venues.length; i++) {
														
														$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
														
													}  
													$('#amenities').append('</table></div>');
													
													
												});
												
												
												 $.ajax({
													url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=hotel',
													dataType: "json"
												}).success(function(data){
													//$('#amenities').append(JSON.stringify(data));
													//alert(JSON.stringify(data));	
													//alert(JSON.stringify(data));
													$('#amenities').append('<br><strong>Hotels</strong><br>');
													var links = JSON.parse(JSON.stringify(data));
													 for (var i = 0; i < links.response.venues.length; i++) {
														
														$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
														
													}  
													$('#amenities').append('</table></div>');
													
													
												});
												
												$.ajax({
													url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=bank',
													dataType: "json"
												}).success(function(data){
													//$('#amenities').append(JSON.stringify(data));
													//alert(JSON.stringify(data));	
													//alert(JSON.stringify(data));
													$('#amenities').append('<br><strong>Banks</strong><br>');
													var links = JSON.parse(JSON.stringify(data));
													 for (var i = 0; i < links.response.venues.length; i++) {
														
														$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
														
													}  
													$('#amenities').append('</table></div>');
													
													
												});
												
												
												$.ajax({
													url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=m-pesa',
													dataType: "json"
												}).success(function(data){
													//$('#amenities').append(JSON.stringify(data));
													//alert(JSON.stringify(data));	
													//alert(JSON.stringify(data));
													$('#amenities').append('<br><strong>M-Pesa Shops</strong><br>');
													var links = JSON.parse(JSON.stringify(data));
													 for (var i = 0; i < links.response.venues.length; i++) {
														
														$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
														
													}  
													$('#amenities').append('</table></div>');
													
													
												});
												
												$.ajax({
													url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=police',
													dataType: "json"
												}).success(function(data){
													//$('#amenities').append(JSON.stringify(data));
													//alert(JSON.stringify(data));	
													//alert(JSON.stringify(data));
													$('#amenities').append('<br><strong>Police Station</strong><br>');
													var links = JSON.parse(JSON.stringify(data));
													 for (var i = 0; i < links.response.venues.length; i++) {
														
														$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
														
													}  
													$('#amenities').append('</table></div>');
													
													
												});
												
												$.ajax({
													url: 'https://api.foursquare.com/v2/venues/search?ll='+ $("#lat").val()+','+ $("#lng").val()+'&oauth_token=EZXDGS3YI3ROJWTW130MBZ0135FL4N2VS1HH5O1QADMGR2KD&v='+today+'&radius=500&query=nakumatt',
													dataType: "json"
												}).success(function(data){
													//$('#amenities').append(JSON.stringify(data));
													//alert(JSON.stringify(data));	
													//alert(JSON.stringify(data));
													$('#amenities').append('<br><strong>Supermarket</strong><br>');
													var links = JSON.parse(JSON.stringify(data));
													 for (var i = 0; i < links.response.venues.length; i++) {
														
														$('#amenities').append('<tr><td><a href="#" onclick="alert(' + links.response.venues[i].name + ')">' + links.response.venues[i].name + '</a>    </td><td> '+ links.response.venues[i].location.distance + 'metres away.</td></tr>');
														
													}  
													$('#amenities').append('</table></div>');
													
													
												});
												
												$('html,body').animate({
														scrollTop: $(".amenities").offset().top},
														'slow');
												});
												
												
												
												
												
											  });
											</script>
										
							</section>

								


		<!-- Scripts -->

			<script src="../assets/js/jquery.scrollex.min.js"></script>
			<script src="../assets/js/jquery.scrolly.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="../assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>

	</body>
</html>