<!DOCTYPE HTML>

<html>
	<head>
		<title>nrb.city | Search for places in Nairobi</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="nrb.city is an online platform that leverages on the power of crowdsourcing traffic, weather, news and event information from social media sites and third party applications and puts them in one place. Easily find something of interest in Nairobi, ranging from the stock exchange values, to the current news, to the events lined up. Nairobi just got simpler." />
		<meta name="keywords" content="Nairobi, Nairobi News, Events in Nairobi, Weather in Nairobi, Nairobi Weather, Kenya, Finance In Kenya, Traffic in Nairobi, Traffic Jam In Nairobi, Matatu Culture, Nairobi Culture" />
		<meta name="author" content="Black | Dev & Design - http://black.co.ke" />
		<meta name="ROBOTS" content="INDEX, FOLLOW" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" type="text/css" href="styles.css" />
		<link rel="stylesheet" href="../assets/css/main.css" />

		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
					<span class="logo"><img src="../images/logo_smallest.png" alt="nrb.city | Nairobi City" /></span>
						<h1>nrb.city</h1>
						<p>Search For Places</p>
					</header>
					<nav id="nav">
						<ul>
							<li><a href="../" >Home</a></li>
							<li><a href="?view=directions" >Directions</a></li>
							<li><a href="#content" >Search Map</a></li>
							<li><a href="#find" >Find In Nairobi</a></li>
							<li><a href="#amenities" >Search Results</a></li>
							
						</ul>
					</nav>

				<!-- Main -->
					<div id="main">

						<!-- Content -->
							<section id="content" class="main">

								<!-- Text -->
									
										<h2>Find Places in Nairobi</h2>
										
										<br>
										<form id="calculate-route" name="calculate-route" action="#" method="get">
										<input id="geocomplete" type="text" placeholder="Type in an address to find" value="Nairobi" />
											  <br>
									
										  <label for="from">From:</label>
										  <input type="text" id="from" name="from" required="required" placeholder="An address" size="30" />
										  <a id="from-link" href="#">Get my position</a>
										  <br />

										  <label for="to">To:</label>
										  <input type="text" id="to" name="to" required="required" placeholder="Another address" size="30" />
										  <a id="to-link" href="#">Get my position</a>
										  <br />
										  
										  <label for="mode">Travel Mode:</label>
										  <select id="mode" name="mode" required="required">
										  <option value="DRIVING">Driving</option>
										  <option value="TRANSIT">Public Means</option>
										  <option value="BICYCLING">Cycling</option>
										  <option value="WALKING">Walking</option>
										  
										  </select>
										  
										  <br />

										  <input type="submit" />
										  <input type="reset" />
										</form>
										<div class="map_canvas" id="map"></div>
										<p id="error"></p>
										<div id="amenities"></div>
										<script src="http://maps.google.com/maps/api/js?key=AIzaSyDtUu3Mg6Vy7b2lhsub3iptL4Cj9WiiM8g"></script>
										<script src="../assets/js/jquery.min.js"></script>
										<script src="../assets/js/jquery.geocomplete.js"></script>
										<script>
										  function calculateRoute(from, to) {
											 var selectedMode = document.getElementById('mode').value;
											var myOptions = {
											  zoom: 10,
											  center: new google.maps.LatLng(40.84, 14.25),
											  mapTypeId: google.maps.MapTypeId.ROADMAP
											};
											// Draw the map
											var mapObject = new google.maps.Map(document.getElementById("map"), myOptions);

											var directionsService = new google.maps.DirectionsService();
																						//alert(selectedMode);

											var directionsRequest = {
											  origin: from,
											  destination: to,
											  //travelMode: google.maps.DirectionsTravelMode.TravelMode[selectedMode],
											  travelMode: google.maps.TravelMode[selectedMode],
											  unitSystem: google.maps.UnitSystem.METRIC
											};
											directionsService.route(
											  directionsRequest,
											  function(response, status)
											  {
												if (status == google.maps.DirectionsStatus.OK)
												{
												  new google.maps.DirectionsRenderer({
													map: mapObject,
													directions: response
												  }).setPanel(document.getElementById('amenities'));
												}
												else
												  $("#error").append("Unable to retrieve your route<br />");
											  }
											);

										  }
								
								
										  

										  $(document).ready(function() {
											// If the browser supports the Geolocation API
											if (typeof navigator.geolocation == "undefined") {
											  $("#error").text("Your browser doesn't support the Geolocation API");
											  return;
											}
										
											

											$("#from-link, #to-link").click(function(event) {
											  event.preventDefault();
											  var addressId = this.id.substring(0, this.id.indexOf("-"));

											  navigator.geolocation.getCurrentPosition(function(position) {
												var geocoder = new google.maps.Geocoder();
												geocoder.geocode({
												  "location": new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
												},
												function(results, status) {
												  if (status == google.maps.GeocoderStatus.OK)
													$("#" + addressId).val(results[0].formatted_address);
												  else
													$("#error").append("Unable to retrieve your address<br />");
												});
											  },
											  function(positionError){
												$("#error").append("Error: " + positionError.message + "<br />");
											  },
											  {
												enableHighAccuracy: true,
												timeout: 10 * 1000 // 10 seconds
											  });
											});

											$("#calculate-route").submit(function(event) {
											  event.preventDefault();
											  calculateRoute($("#from").val(), $("#to").val());
											});
										  });
										</script>
						</section>

								


		<!-- Scripts -->

			<script src="../assets/js/jquery.scrollex.min.js"></script>
			<script src="../assets/js/jquery.scrolly.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="../assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>

	</body>
</html>