<!DOCTYPE html>
<html>
  <head>
    <title>nrb.city | Search for Places in Nairobi</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="nrb.city is an online platform that leverages on the power of crowdsourcing traffic, weather, news and event information from social media sites and third party applications and puts them in one place. Easily find something of interest in Nairobi, ranging from the stock exchange values, to the current news, to the events lined up. Nairobi just got simpler." />
		<meta name="keywords" content="Nairobi, Nairobi News, Events in Nairobi, Weather in Nairobi, Nairobi Weather, Kenya, Finance In Kenya, Traffic in Nairobi, Traffic Jam In Nairobi, Matatu Culture, Nairobi Culture" />
		<meta name="author" content="Black | Dev & Design - http://black.co.ke" />
		<meta name="ROBOTS" content="INDEX, FOLLOW" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" type="text/css" href="styles.css" />
		<link rel="stylesheet" href="../assets/css/main.css" />

		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<link rel="shortcut icon" href="../images/logo_smallest.png"/>
  </head>
  <body>
  <!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
					<span class="logo"><img src="../images/logo_smallest.png" alt="nrb.city | Nairobi City" /></span>
						<h1>nrb.city</h1>
						<p>Search For Directions</p>
					</header>
					<nav id="nav">
						<ul>
							<li><a href="../" >Home</a></li>
							<li><a href="directions_all.php" >Directions</a></li>
							<li><a href="#content" >Search Map</a></li>
							<li><a href="#find" >Find In Nairobi</a></li>
							<li><a href="#amenities" >Search Results</a></li>
							
						</ul>
					</nav>
					<div id="main">

						<!-- Content -->
							<section id="content" class="main">

								<!-- Text -->
							<h2>Find Places in Nairobi</h2>
							<input id="origin-input" class="controls" type="text"	placeholder="Enter an origin location">

							<input id="destination-input" class="controls" type="text" placeholder="Enter a destination location">

							<div id="mode-selector" class="controls">
							  <input type="radio" name="type" id="changemode-walking" >
							  <label for="changemode-walking">Walking</label>

							  <input type="radio" name="type" id="changemode-transit" checked="checked">
							  <label for="changemode-transit">Bus/Matatu</label>

							  <input type="radio" name="type" id="changemode-driving">
							  <label for="changemode-driving">Driving</label>
							</div>

							<div id="map" class="map_canvas"></div>
							<div id="amenities"></div>
							
						<script src="../assets/js/jquery.min.js"></script>
						<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtUu3Mg6Vy7b2lhsub3iptL4Cj9WiiM8g&libraries=places&callback=initMap" async defer></script>
						<script>
						  // This example requires the Places library. Include the libraries=places
						  // parameter when you first load the API. For example:
						  // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

						  function initMap() {
							var origin_place_id = null;
							var destination_place_id = null;
							var travel_mode = 'TRANSIT';
							var map = new google.maps.Map(document.getElementById('map'), {
							  mapTypeControl: false,
							  center: {lat: -1.29, lng: 36.8},
							  zoom: 13
							});
							var directionsService = new google.maps.DirectionsService;
							var directionsDisplay = new google.maps.DirectionsRenderer;
							directionsDisplay.setMap(map);

							var origin_input = document.getElementById('origin-input');
							var destination_input = document.getElementById('destination-input');
							var modes = document.getElementById('mode-selector');

							map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
							map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);
							map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);

							var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
							origin_autocomplete.bindTo('bounds', map);
							var destination_autocomplete =
								new google.maps.places.Autocomplete(destination_input);
							destination_autocomplete.bindTo('bounds', map);

							// Sets a listener on a radio button to change the filter type on Places
							// Autocomplete.
							function setupClickListener(id, mode) {
							  var radioButton = document.getElementById(id);
							  radioButton.addEventListener('click', function() {
								travel_mode = mode;
							  });
							}
							setupClickListener('changemode-walking', 'WALKING');
							setupClickListener('changemode-transit', 'TRANSIT');
							setupClickListener('changemode-driving', 'DRIVING');

							function expandViewportToFitPlace(map, place) {
							  if (place.geometry.viewport) {
								map.fitBounds(place.geometry.viewport);
							  } else {
								map.setCenter(place.geometry.location);
								map.setZoom(17);
							  }
							}

							origin_autocomplete.addListener('place_changed', function() {
							  var place = origin_autocomplete.getPlace();
							  if (!place.geometry) {
								window.alert("Autocomplete's returned place contains no geometry");
								return;
							  }
							  expandViewportToFitPlace(map, place);

							  // If the place has a geometry, store its place ID and route if we have
							  // the other place ID
							  origin_place_id = place.place_id;
							  route(origin_place_id, destination_place_id, travel_mode,
									directionsService, directionsDisplay);
							});

							destination_autocomplete.addListener('place_changed', function() {
							  var place = destination_autocomplete.getPlace();
							  if (!place.geometry) {
								window.alert("Autocomplete's returned place contains no geometry");
								return;
							  }
							  expandViewportToFitPlace(map, place);

							  // If the place has a geometry, store its place ID and route if we have
							  // the other place ID
							  destination_place_id = place.place_id;
							  route(origin_place_id, destination_place_id, travel_mode,
									directionsService, directionsDisplay);
							});

							function route(origin_place_id, destination_place_id, travel_mode,
										   directionsService, directionsDisplay) {
							  if (!origin_place_id || !destination_place_id) {
								return;
							  }
							  directionsService.route({
								origin: {'placeId': origin_place_id},
								destination: {'placeId': destination_place_id},
								travelMode: travel_mode
							  }, function(response, status) {
								if (status === 'OK') {
								  directionsDisplay.setDirections(response);
								  directionsDisplay.setPanel(document.getElementById('amenities'));
								} else {
								  window.alert('Directions request failed due to ' + status);
								}
							  });
							}
						  }
						</script>
						
				</section>
				</div>
				</div>
				<!-- Scripts -->

			<script src="../assets/js/jquery.scrollex.min.js"></script>
			<script src="../assets/js/jquery.scrolly.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="../assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="../assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>
  </body>
</html>