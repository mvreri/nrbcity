<?php

$conn = pg_connect("host=46.101.3.7 port= 5432 dbname=treets_financial user=zerochills password=zer0chills");

//get the approximate time of the tweet
function humanTiming ($time)
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }

}

//get the roads mentioned in a tweet
function get_pre_and_post($needle, $haystack, $separator = " ") {
    $words = explode($separator, $haystack);
    $key = array_search($needle, $words);
    if ($key !== false) {
        if ($key == 0) {
            return $words[1];
        }
        else if ($key == (count($words) - 1)) {
            return $words[$key - 1];
        }
        else {
            return array($words[$key - 1], $words[$key + 1]);
        }
    }
    else {
        return false;
    }
}
/*
$sentence  = "This is Team One Name";
$pre_and_post_array = get_pre_and_post("One", $sentence);

echo $pre_and_post_array[0];

//this will find the places mentioned inn  tweets. Compare this to gtfs data
echo $pre_and_post_array[1];*/



$arr = array();

if (!empty($_POST['keywordf'])) {
	$keywordf = $_POST['keywordf'];
	$result = pg_query($conn,"SELECT t_tweetid id, tweet, tweetdate FROM t_tweets WHERE tweet LIKE '%".strtoupper($keywordf)."%' OR tweet like '%".strtolower($keywordf)."%' ORDER BY id DESC limit 5; ");
	
	if (pg_num_rows($result) > 0) {
		while ($obj = pg_fetch_assoc($result)) {
			extract($obj);
			//$tweet = strstr($tweet, 'Via', true);
			$arr[] = array('id' => $id, 'tweet' => str_replace("-|-","'",preg_replace('/via.*/', '', $tweet)), 'ago' => humanTiming(strtotime($tweetdate)));
		}
	}
}
echo json_encode($arr);
