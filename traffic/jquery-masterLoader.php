<?php 
/**
 * Infinite scrolling
 * 
 * http://tournasdimitrios1.wordpress.com
 * 
 * @copyright Tournas Dimitrios 2012
 * 
 This program is free software: you can redistribute it and/or modify  it under the terms of the GNU General Public License as published by  the Free Software Foundation, either version 3 of the License, or  (at your option) any later version . This program is distributed in the hope that it will be useful ,  but WITHOUT ANY WARRANTY ; without even the implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  GNU General Public License for more details . You should have received a copy of the GNU General Public License  along with this program .  If not , see <http://www.gnu.org/licenses/> .
 
 * @author Tournas Dimitrios <tournasdimitrios@gmail.com>
 * @version 0.4
 * 
 */

/*
prevent direct access to a php include file
http://stackoverflow.com/questions/409496/prevent-direct-access-to-a-php-include-file
*/
if ( strpos(strtolower($_SERVER['SCRIPT_NAME']),strtolower(basename(__FILE__))) ) {
    header("Location: index.php");
    die("Denny access");
	}
//echo $root_url_weather_tweets;
$url = $root_url_weather_tweets.'/api/traffic/tweets';
$aParameter = array('apikey'=> $apikey,'nextpage'=> $nextpage); //parameters to be sent

$params = json_encode($aParameter); //convert param to json string

//headers to be sent optional if no header required skip this and remove curlopt_httpheader thing from curl call
$aHeaders = array(
        'Content-Type'=>'Content-Type:application/json',
        'accept'=>'accept:application/json'
    );

$c = curl_init();

curl_setopt($c, CURLOPT_USERAGENT,  'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:11.0) Gecko/20100101 Firefox/11.0'); // empty user agents probably not accepted
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($c, CURLOPT_AUTOREFERER,    1);
curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt($c, CURLOPT_HTTPHEADER, $aHeaders);

curl_setopt($c, CURLOPT_URL, $url );
curl_setopt($c, CURLOPT_REFERER, $url);
curl_setopt($c, CURLOPT_POST, true);
curl_setopt($c,CURLOPT_POSTFIELDS,$params);
$response = curl_exec($c);
//echo $response;


$json = json_decode($response, true);
?>
<!-- Main -->
<?php foreach($json['user-data'] as $item) { ?>
					<div id="main" class='postedComment' id="<?php echo $item['tweetid']; ?>">

						<!-- Introduction -->
						
							<section id="intro" class="main">
								<div class="spotlight">
									<div class="content">
										<header class="major">
											<h2>Ipsum sed adipiscing</h2>
										</header>
										
										<ul class="actions">
											<li><a href="generic.html" class="button">Learn More</a></li>
										</ul>
									</div>
									<!--tweets-->
									
									<?php
										$url = "https://publish.twitter.com/oembed?url=https://twitter.com/".str_replace("-|-","'",$item['handle'])."/status/".$item['tweetid'];
										$json = file_get_contents($url);
										$json_data = json_decode($json, true);
										echo $json_data["html"];

										?>	
								</div>
							</section>

						

					
					</div>
					<br>
					<?php
						}
					?>

?>