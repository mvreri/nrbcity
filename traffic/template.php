<?php
/*
function random()
{
  $phrases = array(
    "640K ought to be enough for anybody",
	"the coders are still making one last hack to improve this code",
	"the bits are breeding",
	"would you prefer ugali, chapati, or matumbo?",
	"pay no attention to the man behind the curtain",
	"a few bits tried to escape, but we caught them",
	"and dream of faster computers",
	"checking the gravitational constant in your locale",
	"go ahead -- hold your breath",
	"look at kip...he's keeping the lights on by running on the spot",
	"hum something loud while others stare",
	"the server is powered by a lemon and two electrodes",
	"we love you just the way you are",
	"while a larger software vendor in Seattle takes over the world",
	"we're testing your patience",
	"why don't you order a sandwich?",
	"while the satellite moves into position",
	"the bits are flowing slowly today",
	"it's still faster than you could draw it"
  );

  return $phrases[array_rand($phrases)];
}*/
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>nrb.city</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta name="description" content="nrb.city is an online platform that leverages on the power of crowdsourcing traffic, weather, news and event information from social media sites and third party applications and puts them in one place. Easily find something of interest in Nairobi, ranging from the stock exchange values, to the current news, to the events lined up. Nairobi just got simpler." />
		<meta name="keywords" content="Nairobi, Nairobi News, Events in Nairobi, Weather in Nairobi, Nairobi Weather, Kenya, Finance In Kenya, Traffic in Nairobi, Traffic Jam In Nairobi, Matatu Culture, Nairobi Culture" />
		<meta name="author" content="Black | Dev & Design - http://black.co.ke" />
		<meta name="ROBOTS" content="INDEX, FOLLOW" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="../assets/css/level2.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<link rel="shortcut icon" href="../images/logo_smallest.png"/>
	</head>
	<body>


		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header" class="alt">
						<span class="logo"><a href="../"><img src="../images/logo_smallest.png" alt="nrb.city | Nairobi City" /></a></span>
						<h1>nrb.city</h1>
						<h2><a href="../#traffic">traffic.tweets</a></h2>
						
					</header>

			
				<?php require_once $content; ?>

				<!-- Footer -->
					<footer id="footer">
						<section>
							<h2>Feedback</h2>
							<p>Have any suggestions, contributions, feedback or recommendations? We'd love to hear from you. </p>
							<ul class="actions">
								<li><a href="#" class="button">Contact Us</a></li>
							</ul>
						</section>
						<section>
							<h2>Contacts</h2>
							<dl class="alt">
								<dt>Address</dt>
								<dd>Westlands Business Centre, Nairobi</dd>
								<dt>Phone</dt>
								<dd>+254 795 11 40 43</dd>
								<dt>Email</dt>
								<dd><a href="#">info@nrb.city</a></dd>
							</dl>
							<ul class="icons">
								<li><a href="#" class="icon fa-twitter alt"><span class="label">Twitter</span></a></li>
								<li><a href="#" class="icon fa-facebook alt"><span class="label">Facebook</span></a></li>
								<li><a href="#" class="icon fa-instagram alt"><span class="label">Instagram</span></a></li>
								<li><a href="#" class="icon fa-github alt"><span class="label">GitHub</span></a></li>
								<li><a href="#" class="icon fa-dribbble alt"><span class="label">Dribbble</span></a></li>
							</ul>
						</section>
						<p class="copyright">&copy; nrb.city by <a href="https://black.co.ke">Black | Dev & Design</a>.</p>
					</footer>

			</div>

		<!-- Scripts -->
		
			<script src="../assets/js/jquery.min.js"></script>
			<script src="../assets/js/jquery.scrollex.min.js"></script>
			<script src="../assets/js/jquery.scrolly.min.js"></script>
			<script src="../assets/js/skel.min.js"></script>
			<script src="../assets/js/util.js"></script>
			
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="../assets/js/main.js"></script>
			<script type="text/javascript" src="../assets/js/custom_infinite.js"></script>
			

	</body>
</html>