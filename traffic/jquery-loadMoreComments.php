<?php 
require_once '../library/database.php';

if (isset($_GET['lastComment'])) {
	$lastComment = $_GET['lastComment'];
	$queryString = "&lastComment=$lastComment";
	//echo "----------".$lastComment;
} else {
	$lastComment = '';
	$queryString = '';
	header('Location: index.php?view=timeline&error=' . urlencode('No lastComment ')); 
}
	
$filtered = filter_input(INPUT_GET, "lastComment", FILTER_SANITIZE_URL);
//echo $root_url_weather_tweets;
$url = $root_url_weather_tweets.'/api/traffic/tweets-more';
$aParameter = array('apikey'=> $apikey,'nextpage'=> $filtered); //parameters to be sent

$params = json_encode($aParameter); //convert param to json string

//headers to be sent optional if no header required skip this and remove curlopt_httpheader thing from curl call
$aHeaders = array(
        'Content-Type'=>'Content-Type:application/json',
        'accept'=>'accept:application/json'
    );

$c = curl_init();

curl_setopt($c, CURLOPT_USERAGENT,  'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:11.0) Gecko/20100101 Firefox/11.0'); // empty user agents probably not accepted
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($c, CURLOPT_AUTOREFERER,    1);
curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt($c, CURLOPT_HTTPHEADER, $aHeaders);

curl_setopt($c, CURLOPT_URL, $url );
curl_setopt($c, CURLOPT_REFERER, $url);
curl_setopt($c, CURLOPT_POST, true);
curl_setopt($c,CURLOPT_POSTFIELDS,$params);
$response = curl_exec($c);
//echo $response;


$json = json_decode($response, true);
?>
<!-- Main -->

					<div id="postedComments">
					<?php foreach($json['user-data'] as $item) { ?>

						<!-- Introduction -->
						
							<section id="intro" class="main">
								<div  class='postedComment' id="<?php echo $item['tid']; ?>">
									<div class="content">
										<header class="major">
											<h2><?php echo str_replace("-|-","'",$item['tweet']); ?></h2>
										</header>
										
										<ul class="actions">
											<li><a href="#" class="button"> @<?php echo $item['handle']; ?></a></li>
										</ul>
									</div>
									<!--tweets-->
									
									<?php
										$url = "https://publish.twitter.com/oembed?url=https://twitter.com/".str_replace("-|-","'",$item['handle'])."/status/".$item['tweetid'];
										$json = file_get_contents($url);
										$json_data = json_decode($json, true);
										echo $json_data["html"];

										?>	
								</div>
							</section>

						

						<br>
					<?php
						}
					?>
					</div>
					
					