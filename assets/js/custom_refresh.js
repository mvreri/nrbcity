				$(document).ready(function() {
					$('#keyword').on('input', function() {
						var searchKeyword = $(this).val();
						if (searchKeyword.length >= 2) {
							$.post('search.php', { keywords: searchKeyword }, function(data) {
								$('ul#content').empty()
								$.each(data, function() {
									$('ul#content').append('<a href="searchnairobi/?id=' + this.id + '">' + this.tweet.replace("-|-","'") + ' (' + this.ago + ' ago)' + '</a><br/><br/>');
								});
							}, "json");
						}
					});
					$('#keywordfinance').on('input', function() {
						var searchKeyword = $(this).val();
						if (searchKeyword.length >= 2) {
							$.post('search_finance.php', { keywordf: searchKeyword }, function(data) {
								$('ul#contentfinance').empty()
								$.each(data, function() {
									$('ul#contentfinance').append('<a href="searchnairobi/?id=' + this.id + '">' + this.tweet.replace("-|-","'") + ' (' + this.ago + ' ago)' + '</a><br/><br/>');
								});
							}, "json");
						}
					});
					
					var auto_refresh = setInterval(
					function ()
					{
					$('#finance').load('index_tweets_finance.php').fadeIn("slow");
					$('#traffic').load('index_tweets_traffic.php').fadeIn("slow");
					$('#weather').load('index_weather.php').fadeIn("slow");
					}, 5000);
					


					
				});

				$(document).ajaxStart(function(){
								$('#loading').show();
							 }).ajaxStop(function(){
								$('#loading').hide();
							 });
							 
				 