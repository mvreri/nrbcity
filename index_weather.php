<?php 
//echo $root_url;
require_once 'library/config.php';
require_once 'library/database.php';
$url = $root_url_weather_tweets.'/api/weather/conditions';
$aParameter = array('apikey'=> $apikey); //parameters to be sent

$params = json_encode($aParameter); //convert param to json string

//headers to be sent optional if no header required skip this and remove curlopt_httpheader thing from curl call
$aHeaders = array(
        'Content-Type'=>'Content-Type:application/json',
        'accept'=>'accept:application/json'
    );

$c = curl_init();

curl_setopt($c, CURLOPT_USERAGENT,  'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:11.0) Gecko/20100101 Firefox/11.0'); // empty user agents probably not accepted
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($c, CURLOPT_AUTOREFERER,    1);
curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt($c, CURLOPT_HTTPHEADER, $aHeaders);

curl_setopt($c, CURLOPT_URL, $url );
curl_setopt($c, CURLOPT_REFERER, $url);
curl_setopt($c, CURLOPT_POST, true);
curl_setopt($c,CURLOPT_POSTFIELDS,$params);
$response = curl_exec($c);
//echo $response;


$json = json_decode($response, true);
?>
<header class="major">
								<h2>Weather</h2>
									<p>The weather forecast for today, the <?php date_default_timezone_set('Africa/Nairobi'); echo date("jS F, Y", strtotime(date('d-m-Y'))); ?>, in Nairobi.
							</header>

												
	<?php 
	//converting time from string to readable time
	function convert_army_to_regular($time) {
		$hours = substr($time, 0, 2);
		$minutes = substr($time, 2, 2);

		if ($hours > 11) { 
			if ($hours ==12){
				//take care of 12 midday
				$hours = 12;
				$ampm = 'PM';
			}else{
				$hours = $hours - 12;
				$ampm = 'PM';
			}
		} else {
			if ($hours != 11) {
				$hours = substr($hours, 1, 1);
			}
			$ampm = 'AM';
		}
		return $hours . ':' . $minutes . $ampm;
	}
		
		foreach($json['user-data'] as $item) { ?>
		<ul class="statistics">
			<li class="style1">
				<span class="icon fa-clock-o"></span><br>
				Time: <strong><?php echo  convert_army_to_regular(sprintf('%04u', $item['fortime'])); ?></strong>
			</li>
			<li class="style2">
				<span class="icon fa-sun-o"></span><br>
				Temperatures of: <strong><?php echo $item['tempc']; ?>&deg;c</strong>
			</li>
			<li class="style3">
				<span class="icon fa-meh-o"></span><br>
				 Feels Like: <strong><?php echo $item['feelslikec']; ?>&deg;c</strong> 
			</li>
			<li class="style4">
				<span class="icon fa-long-arrow-right"></span><br>
				Wind Speed: <strong><?php echo $item['windspeed']; ?> km/h</strong> 
			</li>
			<li class="style5">
				<span class="icon fa-umbrella"></span><br>
				Approx. Rainfall: <strong><?php echo $item['rainfall']; ?> mm</strong> 
			</li>
			
			</ul>
		<?php
		}
		?>
</table>
<p >The data displayed here is acquired from <a href="www.worldweatheronline.com/" target="_blank">World Weather Online</a> APIs. It is collected for research by <a href="" target="_blank">Surreal Data</a>.</p>

<footer class="major">
	<ul class="actions">
		<li><a href="#" class="button">Graph This</a></li>
	</ul>
</footer>