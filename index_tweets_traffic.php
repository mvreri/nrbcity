<?php 
//echo $root_url;
require_once 'library/config.php';
require_once 'library/database.php';
$url = $root_url_weather_tweets.'/api/traffic/tweets';
$aParameter = array('apikey'=> $apikey); //parameters to be sent

$params = json_encode($aParameter); //convert param to json string

//headers to be sent optional if no header required skip this and remove curlopt_httpheader thing from curl call
$aHeaders = array(
        'Content-Type'=>'Content-Type:application/json',
        'accept'=>'accept:application/json'
    );

$c = curl_init();

curl_setopt($c, CURLOPT_USERAGENT,  'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:11.0) Gecko/20100101 Firefox/11.0'); // empty user agents probably not accepted
curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($c, CURLOPT_AUTOREFERER,    1);
curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

curl_setopt($c, CURLOPT_HTTPHEADER, $aHeaders);

curl_setopt($c, CURLOPT_URL, $url );
curl_setopt($c, CURLOPT_REFERER, $url);
curl_setopt($c, CURLOPT_POST, true);
curl_setopt($c,CURLOPT_POSTFIELDS,$params);
$response = curl_exec($c);
//echo $response;


$json = json_decode($response, true);
?>
<header class="major">
	<h2>Traffic</h2>
</header>
<table>
		
												
	<?php 
		//removing the links in tweets
		 function linkify($value, $protocols = array('http', 'mail'), array $attributes = array(), $mode = 'normal')
			{
				// Link attributes
				$attr = '';
				foreach ($attributes as $key => $val) {
					$attr = ' ' . $key . '="' . htmlentities($val) . '"';
				}

				$links = array();

				// Extract existing links and tags
				$value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);

				// Extract text links for each protocol
				foreach ((array)$protocols as $protocol) {
					switch ($protocol) {
						case 'http':
						case 'https': $value = preg_replace_callback($mode != 'all' ? '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i' : '~([^\s<]+\.[^\s<]+)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, '<a' . $attr . ' href="' . $protocol . '://' . $link . '" target="_blank">' . $link . '</a>') . '>'; }, $value); break;
						case 'mail': $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, '<a' . $attr . ' href="mailto:' . $match[1] . '">' . $match[1] . '</a>') . '>'; }, $value); break;
						case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, '<a' . $attr . ' href="https://twitter.com/' . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1] . '">' . $match[0] . '</a>') . '>'; }, $value); break;
						default: $value = preg_replace_callback($mode != 'all' ? '~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i' : '~([^\s<]+)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, '<a' . $attr . ' href="' . $protocol . '://' . $match[1] . '">' . $match[1] . '</a>') . '>'; }, $value); break;
					}
				}

				// Insert all link
				return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
			}
		foreach($json['user-data'] as $item) { ?>
		<tbody>
			<tr>
				<td align="left"><?php echo preg_replace('/via.*/', '', linkify(str_replace("-|-","'",$item['tweet']))); ?></td>
			</tr>
		<?php
		}
		?>
</table>
<footer class="major">
									<ul class="actions">
										<li><a href="traffic" class="button">See Traffic Tweets</a></li>
									</ul>
								</footer>