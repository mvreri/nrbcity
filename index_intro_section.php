<section id="intro" class="main">
	<div class="spotlight">
		<div class="content">
			<header class="major">
				<h2>About nrb.city</h2>
			</header>
			<p><strong>nrb.city</strong> is a place that provides access to multiple aspect of Nairobi, mainly crowdsourced, and visualized to give a perspective like no other.</p>
			
		</div>
		<span class="image"><img src="images/pic_01.jpg" alt="" /></span>
	</div>
</section>